module.exports = {
  // directory in which bundle is created
  output: "dist",

  // dev server port
  port: 8080,

  // index.html template location,
  // leave undefined to use pre defined template
  template: "index.html",

  storybook: {
    componentsDir: "./src/components",
  },

  entries: {
    index: "src/index.tsx",
  },

  proxy: {
    "/oauth": {
      target: "http://localhost:3000",
      secure: false,
      changeOrigin: true,
    },
    "/tasks": {
      target: "http://localhost:3001",
      secure: false,
      changeOrigin: true,
    },
  },
};
