FROM nginx:alpine

RUN rm /etc/nginx/conf.d/default.conf

WORKDIR /usr/share/nginx/html

COPY ./dist .
COPY conf /etc/nginx

