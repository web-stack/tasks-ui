enum TaskModelEvents {
  CREATE = "create",
  GET_ALL = "get_all",
  MARK_AS_DONE = "mark_as_done",
}

export default TaskModelEvents;
