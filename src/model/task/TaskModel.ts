import { object, string, boolean } from "typed-contracts";
import Validator from "@model/Validator";

export type Task = {
  _id?: string;
  title: string;
  description: string;
  expireDate: string;
  done: boolean;
  archived: boolean;
};

const taskModel = object({
  _id: string.optional,
  title: string,
  description: string,
  expireDate: string,
  done: boolean,
  archived: boolean,
});

export const tasksValidator: Validator<any> = taskModel("task") as Validator<
  any
>;
