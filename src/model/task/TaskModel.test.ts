import { ValidationError } from "typed-contracts";
import { Task, tasksValidator } from "./TaskModel";

describe("TaskModel test", function () {
  it("should return value on correct data", function () {
    const task: Task = {
      title: "some title",
      description: "some description",
      done: false,
      expireDate: new Date().toISOString(),
      archived: false,
    };

    let caughtError = null;
    const validTask = tasksValidator(task);
    if (validTask instanceof ValidationError) {
      caughtError = validTask;
    }

    expect(caughtError).toBeFalsy();
  });

  it("should return ValidationError on incorrect data", function () {
    const task = {
      title: "some title",
      description: "some description",
      done: 0,
      expireDate: new Date(),
    };

    let caughtError = null;
    const validatedTask = tasksValidator(task as any);
    if (validatedTask instanceof ValidationError) {
      caughtError = validatedTask;
    }

    expect(caughtError).toBeTruthy();
  });

  it("should return ValidationError if required field is null", function () {
    const task = {
      title: null,
      description: "some description",
      done: true,
      expireDate: new Date().toISOString(),
    };

    let caughtError = null;
    const validatedTask = tasksValidator(task as any);
    if (validatedTask instanceof ValidationError) {
      caughtError = validatedTask;
    }

    expect(caughtError).toBeTruthy();
  });
});
