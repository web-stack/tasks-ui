import Validator from "@model/Validator";
import { boolean, object, ValidationError } from "typed-contracts";

export interface Settings {
  tasksSortingOptions: {
    doneFirst: boolean;
    mostRecentFirst: boolean;
  };
}

const settingsModel = object({
  tasksSortingOptions: object({
    doneFirst: boolean,
    mostRecentFirst: boolean,
  }),
});

export const settingsValidator: (
  settings: any
) => Settings | ValidationError = settingsModel("settings");
