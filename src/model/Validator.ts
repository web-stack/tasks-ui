import { ValidationError } from "typed-contracts";

type Validator<T> = (model: T) => T | ValidationError;

export default Validator;
