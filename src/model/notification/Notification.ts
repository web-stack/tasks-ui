import { ReactNode } from "react";

type Notification = {
  id: string;
  title: string | ReactNode;
  content: string | ReactNode;
};

export default Notification;
