import PageWrapper from "@components/pageWrapper/PageWrapper";
import Router from "@components/router/Router";
import hot from "@ismithi/ui-builder/hot";
import Providers from "@provider/index";
import React from "react";
import "./App.css";

function App() {
  return (
    <Providers>
      <PageWrapper>
        <Router />
      </PageWrapper>
    </Providers>
  );
}

export default hot(App);
