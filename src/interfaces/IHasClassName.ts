interface IHasClassName {
  className?: string;
}

export default IHasClassName;
