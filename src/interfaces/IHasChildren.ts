import { ReactNode } from "react";

interface IHasChildren {
  children?: ReactNode;
}

export default IHasChildren;
