import IHasChildren from "@interfaces/IHasChildren";
import EventDispatcher, {
  IEventDispatcher,
} from "@utils/event/EventDispatcher";
import React from "react";

const eventDispatcher = new EventDispatcher();

export const EventDispatcherContext = React.createContext<IEventDispatcher>(
  eventDispatcher
);

interface IProps extends IHasChildren {}

function EventDispatcherProvider(props: IProps) {
  return (
    <EventDispatcherContext.Provider value={eventDispatcher}>
      {props.children}
    </EventDispatcherContext.Provider>
  );
}

export default EventDispatcherProvider;
