import IHasChildren from "@interfaces/IHasChildren";
import Notification from "@model/notification/Notification";
import { useLocalStore } from "mobx-react-lite";
import React from "react";

type NotificationsStore = {
  notifications: Notification[];
};

export const NotificationsContext = React.createContext<NotificationsStore>({
  notifications: [],
});

interface IProps extends IHasChildren {}

function NotificationsProvider(props: IProps) {
  const store = useLocalStore<NotificationsStore>(() => ({
    notifications: [],
  }));

  return (
    <NotificationsContext.Provider value={store}>
      {props.children}
    </NotificationsContext.Provider>
  );
}

export default NotificationsProvider;
