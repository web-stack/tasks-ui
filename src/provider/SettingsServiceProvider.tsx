import SettingsService, {
  ISettingsService,
} from "@service/settings/SettingsService";
import React, { ReactNode } from "react";

const settingsService = new SettingsService();

export const SettingsServiceContext = React.createContext<ISettingsService>(
  settingsService
);

interface IProps {
  children: ReactNode;
}

function SettingsServiceProvider({ children }: IProps) {
  return (
    <SettingsServiceContext.Provider value={settingsService}>
      {children}
    </SettingsServiceContext.Provider>
  );
}

export default SettingsServiceProvider;
