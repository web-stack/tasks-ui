import React, { ReactNode } from "react";
import authService from "@service/auth/AuthService";

export const AuthenticationContext = React.createContext(authService);

interface IProps {
  children: ReactNode;
}

function AuthenticationProvider({ children }: IProps) {
  return (
    <AuthenticationContext.Provider value={authService}>
      {children}
    </AuthenticationContext.Provider>
  );
}

export default AuthenticationProvider;
