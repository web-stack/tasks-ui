import { IHasChildren } from "@ismithi/ui-kit/interfaces";
import { ThemeProvider, themes } from "@ismithi/ui-kit/styles";
import AuthenticationProvider from "@provider/AuthenticationProvider";
import SettingsServiceProvider from "@provider/SettingsServiceProvider";
import TasksServiceProvider from "@provider/tasks/TasksServiceProvider";
import React from "react";

function Providers(props: IHasChildren) {
  return (
    <AuthenticationProvider>
      <SettingsServiceProvider>
        <TasksServiceProvider>
          <ThemeProvider theme={themes.defaultTheme}>
            {props.children}
          </ThemeProvider>
        </TasksServiceProvider>
      </SettingsServiceProvider>
    </AuthenticationProvider>
  );
}

export default Providers;
