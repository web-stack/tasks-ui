import { tasksValidator } from "@model/task/TaskModel";
import TasksService, { ITasksService } from "@service/tasks/TasksService";
import TasksServiceApi from "@service/tasks/TasksServiceApi";
import TasksServiceValidator from "@service/tasks/TasksServiceValidator";
import TasksStore from "@store/tasks/TasksStore";

function makeTasksService(): ITasksService {
  const tasksServiceApi = new TasksServiceValidator(
    tasksValidator,
    new TasksServiceApi()
  );
  const tasksStore = new TasksStore();
  return new TasksService(tasksServiceApi, tasksStore);
}

export default makeTasksService;
