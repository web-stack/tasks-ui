import makeTasksService from "@provider/tasks/tasksServiceFactory";
import { ITasksService } from "@service/tasks/TasksService";
import React, { ReactNode } from "react";

const tasksService = makeTasksService();

export const TasksServiceContext = React.createContext<ITasksService>(
  tasksService
);

interface IProps {
  children: ReactNode;
}

function TasksServiceProvider({ children }: IProps) {
  return (
    <TasksServiceContext.Provider value={tasksService}>
      {children}
    </TasksServiceContext.Provider>
  );
}

export default TasksServiceProvider;
