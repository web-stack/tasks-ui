import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "mobx-react-lite/batchingForReactDom";

const root = document.getElementById("root");

const renderApp = () => ReactDOM.render(<App />, root);

renderApp();

if (module.hot) module.hot.accept("./App", () => renderApp());
