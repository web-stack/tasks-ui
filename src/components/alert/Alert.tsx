import React, { useEffect } from "react";

import { Modal } from "@ismithi/ui-kit/components";
import clsx from "clsx";
import styles from "./Alert.styles.scss";

interface IProps {
  type?: "info" | "error";
  isOpen: boolean;
  onClose?: () => void;
  timeout?: number;
  title: string;
  description?: string;
}

function Alert(props: IProps) {
  useEffect(() => {
    let timeout: any;
    if (props.isOpen) {
      timeout = setTimeout(() => {
        if (props.onClose) {
          props.onClose();
        }
      }, props.timeout || 3000);
    }

    return () => {
      if (timeout) clearTimeout(timeout);
    };
  }, [props.isOpen]);

  return (
    <Modal>
      <div
        className={clsx(
          styles.root,
          props.isOpen ? styles.open : styles.closed
        )}
      >
        <div
          className={clsx(
            styles.container,
            props.type === "error" && styles.error
          )}
        >
          <h4 className={styles.title}>{props.title}</h4>
          <p className={styles.description}>{props.description}</p>
        </div>
      </div>
    </Modal>
  );
}

export default Alert;
