import { observer } from "mobx-react-lite";
import React, { useEffect, useState } from "react";
import { AuthenticationContext } from "@provider/AuthenticationProvider";
import { AuthService } from "@service/auth/AuthService";
import { HashRouter, Switch, Route } from "react-router-dom";
import AuthenticationPage from "@pages/authentication/AuthenticationPage";
import HomePage from "@pages/home/HomePage";

interface IRouter {
  readonly route: string;
  setRoute(route: string): void;
}

export const RouterContext = React.createContext<IRouter | null>(null);

function Router() {
  const authService = React.useContext(AuthenticationContext);

  useEffect(() => {
    (async () => {
      await authService.isAuthenticated();
    })();
  }, []);

  return (
    <HashRouter>
      {authService.authenticated === null ? (
        <></>
      ) : authService.authenticated ? (
        <Switch>
          <Route path="/" component={HomePage} />
        </Switch>
      ) : (
        <AuthenticationPage />
      )}
    </HashRouter>
  );
}

export default observer(Router);
