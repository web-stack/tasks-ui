import React from "react";
import styles from "./PlusButton.styles.scss";

interface IProps {
  onClick?: () => void;
}

function PlusButton(props: IProps) {
  return (
    <div className={styles.fab} onClick={props.onClick}>
      <div className={styles.plus} />
    </div>
  );
}

export default PlusButton;
