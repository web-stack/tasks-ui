import Icon from "@components/icon/Icon";
import TaskModelEvents from "@model/task/TaskModelEvents";
import { EventDispatcherContext } from "@provider/EventDispatcherProvider";
import React, { useContext } from "react";

interface IProps {
  taskId: string | undefined;
  onClick?: () => void;
}

function CompleteButton(props: IProps) {
  const eventDispatcher = useContext(EventDispatcherContext);
  const handleClick = () => {
    if (props.onClick) props.onClick();

    eventDispatcher.dispatch({
      name: TaskModelEvents.MARK_AS_DONE,
      payload: props.taskId,
    });
  };

  return <Icon name="cross" onClick={handleClick} />;
}

export default CompleteButton;
