import Icon from "@components/icon/Icon";
import CompleteButton from "@components/taskCard/completeButton/CompleteButton";
import IHasClassName from "@interfaces/IHasClassName";
import { Box, FlexBox } from "@ismithi/ui-kit/components";
import { Task } from "@model/task/TaskModel";
import cardStyles from "@styles/Card.styles.scss";
import dateToTextBasedFormat from "@utils/dateToTextBasedFormat";
import { observer } from "mobx-react-lite";
import React from "react";
import styles from "./TaskCard.styles.scss";

interface IProps extends IHasClassName {
  task: Task;
}

function TaskCard(props: IProps) {
  const date = dateToTextBasedFormat(new Date(props.task.expireDate));

  return (
    <article className={props.className}>
      <div className={cardStyles.cardRoot}>
        <FlexBox wrap={false} spacing={2}>
          {!props.task.archived ? (
            props.task.done ? (
              <Icon name="doneMark" />
            ) : (
              <CompleteButton taskId={props.task._id} />
            )
          ) : (
            <></>
          )}
          <Box px={4} grow>
            <h4 className={styles.title}>{props.task.title}</h4>
            <p className={styles.description}>{props.task.description}</p>
          </Box>
          <div>
            <p className={styles.expireDate}>{date}</p>
          </div>
        </FlexBox>
      </div>
    </article>
  );
}

export default observer(TaskCard);
