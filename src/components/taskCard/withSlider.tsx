import React, { ComponentType, useState } from "react";
import { animated, useSpring } from "react-spring";
import { useGesture } from "react-use-gesture";

interface WithSliderProps {
  onLeft?: () => void;
  onRight?: () => void;
}

function withSlider<T>(
  Component: ComponentType<T>,
  { threshold } = { threshold: 100 }
) {
  return (props: T & WithSliderProps) => {
    const [removed, setRemoved] = useState(false);
    const [{ dx }, set] = useSpring(() => ({
      dx: 0,
    }));

    const bind = useGesture({
      onDrag: ({ down, movement: [x], velocity }) => {
        if (removed) {
          return;
        }

        const sign = Math.sign(x);
        if (Math.abs(x) >= threshold && velocity >= 1 && !down) {
          if ((sign > 0 && props.onRight) || (sign < 0 && props.onLeft)) {
            setRemoved(true);
            set({ dx: window.innerWidth * sign });
          }

          if (sign > 0 && props.onRight) {
            props.onRight();
            return;
          }

          if (sign < 0 && props.onLeft) {
            props.onLeft();
            return;
          }

          return;
        }

        if ((sign > 0 && props.onRight) || (sign < 0 && props.onLeft))
          set({ dx: down && Math.abs(x) > 15 ? x : 0 });
      },
      onDragEnd: () => {
        if (removed) {
        }
      },
    });

    return (
      <animated.div
        {...bind()}
        style={{ transform: dx.interpolate((dx) => `translateX(${dx}px)`) }}
      >
        <Component {...props} />
      </animated.div>
    );
  };
}

export default withSlider;
