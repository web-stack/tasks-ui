import React, { ReactNode } from "react";
import styles from "./Styles.scss";
import BackgroundLight from "./img/background-light-blue.svg";
import BackgroundDark from "./img/background-dark-blue.svg";

interface IProps {
  children: ReactNode;
}

function PageWrapper({ children }: IProps) {
  return (
    <section className={styles.pageRoot}>
      <BackgroundLight className={styles.backgroundLight} />
      <BackgroundDark className={styles.backgroundDark} />
      {children}
    </section>
  );
}

export default PageWrapper;
