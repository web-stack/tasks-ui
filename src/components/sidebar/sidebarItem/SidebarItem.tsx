import IHasChildren from "@interfaces/IHasChildren";
import { Box, Column, Row } from "@ismithi/ui-kit/components";
import clsx from "clsx";
import React, { ReactNode } from "react";
import styles from "./SidebarItem.styles.scss";

interface IProps extends IHasChildren {
  icon?: ReactNode;
  link?: boolean;
  title: string | ReactNode;
  onClick?: () => void;
}

function SidebarItem(props: IProps) {
  const title =
    typeof props.title === "string" ? (
      <h3 className={styles.title}>{props.title}</h3>
    ) : (
      <>{props.title}</>
    );

  return (
    <Box px={4} py={2} className={clsx(props.link && styles.root)}>
      <Row alignItems="center" spacing={1} onClick={props.onClick}>
        {props.icon ? <>{props.icon}</> : <Box p={4} />}
        {title}
      </Row>
      {props.children && <Box pl={8}>{props.children}</Box>}
    </Box>
  );
}

export default SidebarItem;
