import { Slide } from "@ismithi/ui-kit/components";
import { IHasChildren } from "@ismithi/ui-kit/interfaces";
import React from "react";
import styles from "./SidebarContainer.styles.scss";

interface IProps extends IHasChildren {
  isOpen?: boolean;
  onAnimationEnd?: () => void;
}

function SidebarContainer(props: IProps) {
  return (
    <Slide
      direction="right"
      isOpen={!!props.isOpen}
      keepMounted={false}
      onAnimationEnd={props.onAnimationEnd}
    >
      <div className={styles.root}>{props.children}</div>
    </Slide>
  );
}

export default SidebarContainer;
