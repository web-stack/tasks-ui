import ArchiveItem from "@components/sidebar/items/ArchiveItem";
import LeaveItem from "@components/sidebar/items/LeaveItem";
import SettingsItem from "@components/sidebar/items/SettingsItem";
import SidebarContainer from "@components/sidebar/sidebarContainer/SidebarContainer";
import { Box, Column, Dialog } from "@ismithi/ui-kit/components";
import { IHasChildren } from "@ismithi/ui-kit/interfaces";
import React from "react";

interface IProps extends IHasChildren {
  isOpen: boolean;
  onClose?: () => void;
}

function Sidebar(props: IProps) {
  return (
    <Dialog
      isOpen={props.isOpen}
      onClose={props.onClose}
      Container={SidebarContainer}
      ContainerProps={{ isOpen: props.isOpen }}
      OverlayProps={{ onClick: props.onClose }}
    >
      <Box>
        <Box px={4}>
          <h2>Options</h2>
        </Box>
        <br />
        {props.children && (
          <Column>
            <>{props.children}</>
          </Column>
        )}
      </Box>
    </Dialog>
  );
}

export default Sidebar;
