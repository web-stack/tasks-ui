import Icon from "@components/icon/Icon";
import SidebarItem from "@components/sidebar/sidebarItem/SidebarItem";
import { AuthenticationContext } from "@provider/AuthenticationProvider";
import React, { useContext } from "react";

function LeaveItem() {
  const authService = useContext(AuthenticationContext);
  const handleClick = () => {
    authService.logout();
  };

  return (
    <SidebarItem
      link
      title="Leave"
      onClick={handleClick}
      icon={<Icon name="door" />}
    />
  );
}

export default LeaveItem;
