import Icon from "@components/icon/Icon";
import SidebarItem from "@components/sidebar/sidebarItem/SidebarItem";
import React from "react";
import { useHistory } from "react-router-dom";

function ArchiveItem() {
  const history = useHistory();

  const handleClick = () => {
    history.push("/archive");
  };

  return (
    <SidebarItem
      link
      title="Archive"
      icon={<Icon name="archive" />}
      onClick={handleClick}
    />
  );
}

export default ArchiveItem;
