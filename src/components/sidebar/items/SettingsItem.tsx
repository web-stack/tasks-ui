import Icon from "@components/icon/Icon";
import styles from "@components/sidebar/Sidebar.styles.scss";
import SidebarItem from "@components/sidebar/sidebarItem/SidebarItem";
import { Box, Column, Row, Switch } from "@ismithi/ui-kit/components";
import { SettingsServiceContext } from "@provider/SettingsServiceProvider";
import { observer } from "mobx-react-lite";
import React, { useContext } from "react";

function SettingsItem() {
  const settingsService = useContext(SettingsServiceContext);
  const {
    doneFirst,
    mostRecentFirst,
  } = settingsService.settings.tasksSortingOptions;

  function handleDoneFirstChange(isOn: boolean) {
    settingsService.settings.tasksSortingOptions.doneFirst = isOn;
  }

  function handleMostRecentFirstChange(isOn: boolean) {
    settingsService.settings.tasksSortingOptions.mostRecentFirst = isOn;
  }

  return (
    <SidebarItem title="Settings" icon={<Icon name="gear" />}>
      <Box p={1} />
      <Column spacing={1}>
        <Row alignItems="center" justify="space-between">
          <h5 className={styles.sublist__item_text}>Done first</h5>
          <Switch isOn={doneFirst} onChange={handleDoneFirstChange} />
        </Row>
        <Row alignItems="center" justify="space-between">
          <h5 className={styles.sublist__item_text}>Most recent first</h5>
          <Switch
            isOn={mostRecentFirst}
            onChange={handleMostRecentFirstChange}
          />
        </Row>
      </Column>
    </SidebarItem>
  );
}

export default observer(SettingsItem);
