import Icon from "@components/icon/Icon";
import SidebarItem from "@components/sidebar/sidebarItem/SidebarItem";
import React from "react";
import { useHistory } from "react-router-dom";

function ActiveTasksItem() {
  const history = useHistory();
  const handleClick = () => {
    history.replace("/");
  };

  return (
    <SidebarItem
      link
      title="Active tasks"
      icon={<Icon name="tasks_list" />}
      onClick={handleClick}
    />
  );
}

export default ActiveTasksItem;
