import { Input, IInputProps } from "@ismithi/ui-kit/components";
import React, { useState } from "react";

type DayKeywords = {
  [day: string]: Date;
};

const dayKeywords = (): DayKeywords => {
  const date = new Date();
  const dayOfTheWeek = date.getDay();
  const tomorrowDate = new Date(date);
  tomorrowDate.setDate(date.getDate() + 1);

  const mondayDate = new Date(date);
  mondayDate.setDate(date.getDate() + 8 - dayOfTheWeek);

  const tuesdayDate = new Date(date);
  tuesdayDate.setDate(
    date.getDate() + 9 - dayOfTheWeek - (dayOfTheWeek < 2 ? 7 : 0)
  );

  const wednesdayDate = new Date(date);
  tuesdayDate.setDate(
    date.getDate() + 10 - dayOfTheWeek - (dayOfTheWeek < 3 ? 7 : 0)
  );

  const thursdayDate = new Date(date);
  thursdayDate.setDate(
    date.getDate() + 11 - dayOfTheWeek - (dayOfTheWeek < 4 ? 7 : 0)
  );

  const fridayDate = new Date(date);
  fridayDate.setDate(
    date.getDate() + 12 - dayOfTheWeek - (dayOfTheWeek < 5 ? 7 : 0)
  );

  const saturdayDate = new Date(date);
  saturdayDate.setDate(
    date.getDate() + 13 - dayOfTheWeek - (dayOfTheWeek < 6 ? 7 : 0)
  );

  const sundayDate = new Date(date);
  sundayDate.setDate(
    date.getDate() + 14 - dayOfTheWeek - (dayOfTheWeek < 7 ? 7 : 0)
  );

  return {
    tomorrow: tomorrowDate,
    today: date,
    monday: mondayDate,
    tuesday: tuesdayDate,
    wednesday: wednesdayDate,
    thursday: thursdayDate,
    friday: fridayDate,
    saturday: saturdayDate,
    sunday: sundayDate,
  };
};

interface IProps extends IInputProps {
  onStringParsed?: (date: Date) => void;
}

function TextDateTimeInput({ onStringParsed, ...props }: IProps) {
  const [dateValue, setDateValue] = useState<Date>();
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const date = new Date();
    const words = e.target.value.split(" ");
    words.forEach((word) => {
      const day = dayKeywords()[word.toLowerCase()];

      if (day) {
        date.setDate(day.getDate());
        date.setMonth(day.getMonth());
        date.setFullYear(day.getFullYear());

        if (onStringParsed) {
          onStringParsed(day);
        }
      }
      const time = word.split(":");
      if (time.length === 2 && time[0] && time[1]) {
        date.setHours(parseInt(time[0]));
        date.setMinutes(parseInt(time[1]));
      }
    });

    setDateValue(date);
  };

  return (
    <>
      <Input {...props} onChange={handleChange} name={props.name + "-mask"} />
      <input
        readOnly
        name={props.name}
        type="hidden"
        value={dateValue ? dateValue.toISOString() : new Date().toISOString()}
      />
    </>
  );
}

export default TextDateTimeInput;
