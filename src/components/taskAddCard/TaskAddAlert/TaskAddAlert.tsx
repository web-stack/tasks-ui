import Alert from "@components/alert/Alert";
import React from "react";

export type StateCode = "error" | "success";

interface IProps {
  isOpen: boolean;
  stateCode: StateCode;
  onClose?: () => void;
}

function TaskAddAlert({ isOpen, stateCode, onClose }: IProps) {
  return (
    <Alert
      isOpen={isOpen}
      title={stateCode}
      type={stateCodeToAlertType(stateCode)}
      description={stateCodeToDescription(stateCode)}
      onClose={onClose}
    />
  );
}

export default TaskAddAlert;

function stateCodeToAlertType(code: StateCode): "error" | "info" {
  if (code === "error") return code;

  return "info";
}

function stateCodeToDescription(stateCode: StateCode): string {
  if (stateCode === "error") return "Could not create task 😕";

  return "Task created successfully 😄";
}
