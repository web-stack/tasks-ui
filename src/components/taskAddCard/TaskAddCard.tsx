import { Task, tasksValidator } from "@model/task/TaskModel";
import React, { useContext, useState } from "react";
import { ValidationError } from "typed-contracts";
import { StateCode, TaskAddAlert } from "./TaskAddAlert";

import TaskAddCardLayout from "@components/taskAddCard/TaskAddCard.layout";
import { TasksServiceContext } from "@provider/tasks/TasksServiceProvider";

interface ITaskAddCardState {
  alertIsOpen: boolean;
  code: StateCode;
  message: string;
}

function TaskAddCard() {
  const [state, setState] = useState<ITaskAddCardState>({
    alertIsOpen: false,
    code: "error",
    message: "",
  });

  const tasksService = useContext(TasksServiceContext);
  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const formData: FormData = new FormData(e.target as HTMLFormElement);
    formData.delete("expireDate-mask");

    const task = tasksValidator({
      title: formData.get("title")?.toString(),
      description: formData.get("description")?.toString(),
      expireDate: formData.get("expireDate")?.toString(),
      done: false,
      archived: false,
    });

    if (task instanceof ValidationError) {
      setState({
        code: "error",
        message:
          "Oops, seems like there's an error with task creation. This is probably UI-side error",
        alertIsOpen: true,
      });
    }

    try {
      await tasksService.add(task);

      setState({ code: "success", message: "Done!", alertIsOpen: true });
    } catch (err) {
      setState({ code: "error", message: err.message, alertIsOpen: true });
    }
  };

  const handleAlertClose = () => {
    setState({ ...state, alertIsOpen: false });
  };

  return (
    <>
      <TaskAddCardLayout onSubmit={handleSubmit} />
      <TaskAddAlert
        isOpen={state.alertIsOpen}
        stateCode={state.code}
        onClose={handleAlertClose}
      />
    </>
  );
}

export default TaskAddCard;
