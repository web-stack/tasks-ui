import { ThemeProvider, themes } from "@ismithi/ui-kit/styles";

import React from "react";
import TaskAddCard from "./TaskAddCard";
import { render } from "@ismithi/ui-builder/testing";

describe("TaskAddCard test", function () {
  it("Should render with no errors", async () => {
    render(
      <ThemeProvider theme={themes.defaultTheme}>
        <TaskAddCard />
      </ThemeProvider>
    );
  });
});
