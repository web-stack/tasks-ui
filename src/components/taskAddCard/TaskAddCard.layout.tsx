import TextDateTimeInput from "@components/textDateTimeInput/TextDateTimeInput";
import IHasClassName from "@interfaces/IHasClassName";
import { Button, Input, Column, Textarea } from "@ismithi/ui-kit/components";
import CardStyles from "@styles/Card.styles.scss";
import clsx from "clsx";
import React from "react";

interface IProps extends IHasClassName {
  onSubmit?: (e: React.FormEvent<HTMLFormElement>) => void;
}

function TaskAddCardLayout(props: IProps) {
  return (
    <form onSubmit={props.onSubmit}>
      <div className={clsx(CardStyles.cardRoot, props.className)}>
        <Column spacing={2}>
          <Input label="Quick title" name="title" type="text" />
          <Textarea label="Short description" name="description" />
          <TextDateTimeInput label="Due date" name="expireDate" />
          <Button>Add task</Button>
        </Column>
      </div>
    </form>
  );
}

export default TaskAddCardLayout;
