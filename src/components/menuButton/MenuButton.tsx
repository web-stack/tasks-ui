import Icon from "@components/icon/Icon";
import React from "react";
import styles from "./MenuButton.styles.scss";

interface IProps {
  onClick?: () => void;
}

function MenuButton(props: IProps) {
  return <Icon onClick={props.onClick} className={styles.root} name="menu" />;
}

export default MenuButton;
