import clsx from "clsx";
import React from "react";
import styles from "./Icon.styles.scss";

const getComponent = (iconName: string) => require(`./images/${iconName}.svg`);

interface IProps extends React.SVGProps<SVGSVGElement> {
  name: string;
}

function Icon({ name, ...props }: IProps) {
  const IconSvg = getComponent(name).default;

  return (
    <IconSvg
      {...props}
      className={clsx(styles.root, props.className && props.className)}
    />
  );
}

export default Icon;
