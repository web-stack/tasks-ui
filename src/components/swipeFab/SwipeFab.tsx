import React from "react";
import { useSpring, animated, interpolate } from "react-spring";
import { useDrag } from "react-use-gesture";
import styles from "./SwipeFab.styles.scss";

interface IProps {
  onOpen?: () => void;
}

function SwipeFab(props: IProps) {
  const [{ y }, set] = useSpring(() => ({ y: 0 }));
  const bind = useDrag(({ movement: [_, y], down, cancel }) => {
    if (Math.abs(y) >= 200) {
      if (props.onOpen)
        props.onOpen();
      
      if(cancel)
        cancel();
    }

    set({ y: down ? y : 0 });
  });

  return (
    <>
      <div className={styles.root}>
        <animated.div
          {...bind()}
          className={styles.fab}
          style={{
            transform: interpolate([y], (y) => `translate3d(0, ${y}px, 0)`),
            touchAction: "none",
          }}
        >
          <div className={styles.plus} />
        </animated.div>
      </div>
    </>
  );
}

export default SwipeFab;
