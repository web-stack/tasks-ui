import clsx from "clsx";
import React, { ReactNode } from "react";
import styles from "./Styles.scss";

export interface IButtonProps
  extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  children: ReactNode;
  color?: "primary" | "transparent";
}

function Button({ children, color = "primary", ...rest }: IButtonProps) {
  const classes = clsx(
    styles.root,
    color === "transparent" && styles.color_transparent,
    rest.className
  );

  return (
    <button {...rest} className={classes}>
      {children}
    </button>
  );
}

export default Button;
