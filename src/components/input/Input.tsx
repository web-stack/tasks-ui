import React, { useRef } from "react";
import inputStyles from "./Styles.scss";

export interface IInputProps {
  label: string;
  name: string;
  type?: "text" | "password" | "date" | "datetime-local" | "time";
  disabled?: boolean;
  initial?: string | number | string[];
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

function Input(props: IInputProps) {
  const input = useRef<HTMLInputElement>(null);
  const focusElement = () => {
    input.current?.focus();
  };

  return (
    <div className={inputStyles.input_container}>
      <input
        ref={input}
        type={props.type || "text"}
        name={props.name}
        placeholder=" "
        disabled={props.disabled}
        defaultValue={props.initial}
        onChange={props.onChange}
      />
      <label className={inputStyles.placeholder} onClick={focusElement}>
        {props.label}
      </label>
    </div>
  );
}

export default Input;
