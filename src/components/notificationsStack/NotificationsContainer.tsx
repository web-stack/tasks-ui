import Alert from "@components/alert/Alert";
import Notification from "@components/notificationsStack/notification/Notification";
import NotificationModel from "@model/notification/Notification";
import { observer } from "mobx-react-lite";
import React, { useEffect, useState } from "react";
import styles from "./NotificationsContainer.styles.scss";

interface IProps {
  notifications: NotificationModel[];
}

function NotificationsContainer(props: IProps) {
  const [items, setItems] = useState(props.notifications)
  const [visible, setVisible] = useState([]);
  
  useEffect(() => {
    if (props.notifications.length > items.length) {
    
    }
  }, [props.notifications.length])
  
  return (
    <>
    
    </>
  );
}

export default observer(NotificationsContainer);
