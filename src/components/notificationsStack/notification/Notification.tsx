import NotificationModel from "@model/notification/Notification";
import React from "react";
import styles from "./Notification.styles.scss";

interface IProps {
  data: NotificationModel;
}

function Notification(props: IProps) {
  return (
    <div className={styles.container}>
      <h3>{props.data.title}</h3>
      <p>{props.data.content}</p>
    </div>
  );
}

export default Notification;
