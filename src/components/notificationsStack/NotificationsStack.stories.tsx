import Button from "@components/button/Button";
import NotificationsStack from "@components/notificationsStack/NotificationsStack";
import NotificationsProvider, {
  NotificationsContext,
} from "@provider/NotificationsProvider";
import React, { useContext } from "react";

export default {
  title: "Notifications stack",
};

const Helper = () => {
  const store = useContext(NotificationsContext);

  return (
    <Button
      onClick={() =>
        store.notifications.push({
          id: "test",
          title: "title",
          content: "description",
        })
      }
    >
      Add
    </Button>
  );
};

export const Example = () => (
  <NotificationsProvider>
    <NotificationsStack />
    <Helper />
  </NotificationsProvider>
);
