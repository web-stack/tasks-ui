import Notification from "@components/notificationsStack/notification/Notification";
import NotificationsContainer from "@components/notificationsStack/NotificationsContainer";
import { Modal } from "@ismithi/ui-kit/components";
import { NotificationsContext } from "@provider/NotificationsProvider";
import { observe } from "mobx";
import React, { useContext, useEffect } from "react";

interface IProps {
  timeout?: number;
}

function NotificationsStack({ timeout = 3000 }: IProps) {
  const store = useContext(NotificationsContext);

  useEffect(() => {
    observe(store.notifications, (change) => {
      setTimeout(() => {
        const i = store.notifications.indexOf(change.object);
        store.notifications.splice(i, 1);
      }, timeout);
    });
  }, []);

  return (
    <Modal>
      <NotificationsContainer notifications={store.notifications} />
    </Modal>
  );
}

export default NotificationsStack;
