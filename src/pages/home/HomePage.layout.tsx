import MenuButton from "@components/menuButton/MenuButton";
import PlusButton from "@components/plusButton/PlusButton";
import TaskAddCard from "@components/taskAddCard/TaskAddCard";
import { HomePageContext } from "@pages/home/HomePage";
import styles from "@pages/home/HomePage.styles.scss";
import TasksList from "@pages/home/tasksList/TasksList";
import animations from "@styles/Animations.scss";
import useToggle from "@utils/hooks/useToggle";
import React, { useContext } from "react";
import { Box, Dialog, Row } from "@ismithi/ui-kit/components";

function HomePageLayout() {
  const [isOpen, toggle] = useToggle(false);
  const store = useContext(HomePageContext);

  return (
    <section className={styles.root}>
      <Row justify="space-between" alignItems="center" wrap={false}>
        <h2 className={animations.fadeAndGrowIn}>My tasks</h2>
        <MenuButton onClick={store.openSidebar} />
      </Row>
      <Box p={2} />
      <section className={styles.tasksList}>
        <TasksList />
      </section>
      <Dialog
        isOpen={isOpen}
        onClose={() => toggle(false)}
        OverlayProps={{ onClick: () => toggle(false) }}
      >
        <div className={styles.taskAddCard_container}>
          <TaskAddCard />
        </div>
      </Dialog>
      <div className={styles.taskAddButton_container}>
        <PlusButton onClick={() => toggle(true)} />
      </div>
    </section>
  );
}

export default HomePageLayout;
