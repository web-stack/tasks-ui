import TaskModelEvents from "@model/task/TaskModelEvents";
import { ITasksService } from "@service/tasks/TasksService";
import DispatchedEvent from "@utils/event/DispatchedEvent";
import EventListener from "@utils/event/EventListener";

class TaskDoneListener implements EventListener<string> {
  readonly name = TaskModelEvents.MARK_AS_DONE;

  private tasksService: ITasksService;

  constructor(tasksService: ITasksService) {
    this.tasksService = tasksService;
  }

  async onMessage(event: DispatchedEvent<string>) {
    await this.tasksService.markAsDone(event.payload);
  }
}

export default TaskDoneListener;
