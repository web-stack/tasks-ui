import { Task } from "@model/task/TaskModel";
import TaskModelEvents from "@model/task/TaskModelEvents";
import { ITasksService } from "@service/tasks/TasksService";
import DispatchedEvent from "@utils/event/DispatchedEvent";
import EventListener from "@utils/event/EventListener";

class AddTaskListener implements EventListener<Task> {
  private tasksService: ITasksService;

  constructor(tasksService: ITasksService) {
    this.tasksService = tasksService;
  }

  readonly name = TaskModelEvents.CREATE;

  onMessage(event: DispatchedEvent<Task>) {
    this.tasksService.add(event.payload);
  }
}
export default AddTaskListener;
