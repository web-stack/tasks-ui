import MenuButton from "@components/menuButton/MenuButton";
import { Box, Row } from "@ismithi/ui-kit/components";
import { HomePageContext } from "@pages/home/HomePage";
import TasksList from "@pages/home/tasksList/TasksList";
import { TasksServiceContext } from "@provider/tasks/TasksServiceProvider";
import animations from "@styles/Animations.scss";
import { observer } from "mobx-react-lite";
import styles from "./Archive.styles.scss";
import React, { useContext, useEffect } from "react";

function Archive() {
  const store = useContext(HomePageContext);
  const tasksService = useContext(TasksServiceContext);

  useEffect(() => {
    tasksService.getArchived();
  }, []);

  return (
    <section className={styles.root}>
      <Row justify="space-between" alignItems="center" wrap={false}>
        <h2 className={animations.fadeAndGrowIn}>Archived tasks</h2>
        <MenuButton onClick={store.openSidebar} />
      </Row>
      <Box p={2} />
      <section className={styles.tasksList}>
        <TasksList tasks={tasksService.archivedTasks} />
      </section>
    </section>
  );
}

export default observer(Archive);
