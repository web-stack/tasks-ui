import Alert from "@components/alert/Alert";
import React from "react";

interface IProps {
  isOpen: boolean;
  onClose?: () => void;
}

function TaskArchivedAlert(props: IProps) {
  return (
    <Alert
      isOpen={props.isOpen}
      title="Success!"
      description="Task archived successfully 😄"
      onClose={props.onClose}
    />
  );
}

export default TaskArchivedAlert;
