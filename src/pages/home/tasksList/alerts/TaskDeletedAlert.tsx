import Alert from "@components/alert/Alert";
import React from "react";

interface IProps {
  isOpen: boolean;
  onClose?: () => void;
}

function TaskDeletedAlert(props: IProps) {
  return (
    <Alert
      isOpen={props.isOpen}
      title="Success!"
      description="Task deleted successfully 🗑️"
      onClose={props.onClose}
    />
  );
}

export default TaskDeletedAlert;
