import TaskCard from "@components/taskCard/TaskCard";
import withSlider from "@components/taskCard/withSlider";
import { Task } from "@model/task/TaskModel";
import TaskArchivedAlert from "@pages/home/tasksList/alerts/TaskArchivedAlert";
import TaskDeletedAlert from "@pages/home/tasksList/alerts/TaskDeletedAlert";
import TaskListItemTransition from "@pages/home/tasksList/TaskListItemTransition";
import {
  sortAndGroupByDone,
  sortByDate,
} from "@pages/home/tasksList/tasksSorting";
import useAlertsState from "@pages/home/tasksList/useAlertsState";
import { SettingsServiceContext } from "@provider/SettingsServiceProvider";
import { TasksServiceContext } from "@provider/tasks/TasksServiceProvider";
import { observer } from "mobx-react-lite";
import React, { useContext, useState } from "react";
import styles from "./TasksList.styles.scss";

const SlidingTaskCard = withSlider(TaskCard);

interface IProps {
  tasks?: Task[];
}

function TasksList(props: IProps) {
  const [mounted, setMounted] = useState(false);
  const [alerts, set] = useAlertsState({ archive: false, remove: false });
  const tasksService = useContext(TasksServiceContext);
  const settingsService = useContext(SettingsServiceContext);
  const {
    doneFirst,
    mostRecentFirst,
  } = settingsService.settings.tasksSortingOptions;

  const tasks = props.tasks || tasksService.tasks;

  const tasksGroup = sortAndGroupByDone([...tasks]);
  const groups = [];
  if (doneFirst) {
    groups.push(tasksGroup.done, tasksGroup.undone);
  } else {
    groups.push(tasksGroup.undone, tasksGroup.done);
  }

  const handleArchive = (task: Task) => () => {
    setTimeout(async () => {
      if (task._id) {
        await tasksService.archive(task._id);
        set("archive")(true)();
      }
    }, 400);
  };

  const handleDelete = (task: Task) => () => {
    setTimeout(async () => {
      if (task._id) {
        await tasksService.deleteTask(task._id);
        set("remove")(true)();
      }
    }, 400);
  };

  const handleAnimationEnd = (i: number, tasks: Task[]) => () => {
    if (i === tasks.length - 1) {
      setTimeout(() => {
        setMounted(true);
      }, 100);
    }
  };

  return (
    <>
      {groups.map((tasks, i) => (
        <React.Fragment key={i}>
          {sortByDate(tasks, { mostRecentFirst }).map((task, i) => (
            <TaskListItemTransition
              index={i}
              key={`${i}:${task._id}`}
              mounted={mounted}
              handleAnimationEnd={handleAnimationEnd(i, tasks)}
            >
              {task.done ? (
                <SlidingTaskCard
                  task={task}
                  onRight={task.archived ? undefined : handleArchive(task)}
                  onLeft={handleDelete(task)}
                  className={styles.taskItem}
                />
              ) : (
                <TaskCard task={task} className={styles.taskItem} />
              )}
            </TaskListItemTransition>
          ))}
        </React.Fragment>
      ))}
      <TaskArchivedAlert
        isOpen={alerts.archive}
        onClose={set("archive")(false)}
      />
      <TaskDeletedAlert isOpen={alerts.remove} onClose={set("remove")(false)} />
    </>
  );
}

export default observer(TasksList);
