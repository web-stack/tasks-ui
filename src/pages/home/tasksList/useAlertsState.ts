import { useState } from "react";

type Mutator = (name: string) => (value: boolean) => () => void;

interface IProps {
  [key: string]: boolean;
}

function useAlertsState(initial: IProps): [IProps, Mutator] {
  const [alerts, setAlerts] = useState(initial);

  const handleChange = (alertName: string) => (value: boolean) => () => {
    setAlerts({ ...alerts, [alertName]: value });
  };

  return [alerts, handleChange];
}

export default useAlertsState;
