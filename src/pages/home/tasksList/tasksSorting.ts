import { Task } from "@model/task/TaskModel";

export function sortByDoneFirst(tasks: Task[]): Task[] {
  return tasks.sort((a, b) => {
    if (a.done && !b.done) {
      return -1;
    }
    if (!a.done && b.done) {
      return 1;
    }

    return 0;
  });
}

export function sortByUnDoneFirst(tasks: Task[]): Task[] {
  return tasks.sort((a, b) => {
    if (a.done && !b.done) {
      return 1;
    }
    if (!a.done && b.done) {
      return -1;
    }

    return 0;
  });
}

type TasksGroup = {
  done: Task[];
  undone: Task[];
};

export function sortAndGroupByDone(tasks: Task[]) {
  const tasksGroup: TasksGroup = {
    done: [],
    undone: [],
  };

  tasks.forEach((task) => {
    if (task.done) {
      tasksGroup.done.push(task);
    } else {
      tasksGroup.undone.push(task);
    }
  });

  return tasksGroup;
}

export function sortByDate(tasks: Task[], { mostRecentFirst = true }) {
  return tasks.sort((a, b) => {
    const dateA = new Date(a.expireDate);
    const dateB = new Date(b.expireDate);

    return mostRecentFirst
      ? dateB.getTime() - dateA.getTime()
      : dateA.getTime() - dateB.getTime();
  });
}
