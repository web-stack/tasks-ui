import IHasChildren from "@interfaces/IHasChildren";
import clsx from "clsx";
import React, { useState } from "react";
import styles from "./TasksList.styles.scss";

interface IProps extends IHasChildren {
  index: number;
  mounted: boolean;
  handleAnimationEnd: () => void;
}

function TaskListItemTransition({
  index,
  children,
  handleAnimationEnd,
  mounted,
}: IProps) {
  const style = !mounted
    ? {
        animationDelay: `${index * 50}ms`,
      }
    : {};

  return (
    <div
      style={style}
      className={clsx(!mounted && styles.taskItem_animation)}
      onAnimationEnd={handleAnimationEnd}
    >
      {children}
    </div>
  );
}

export default TaskListItemTransition;
