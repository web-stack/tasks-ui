import ActiveTasksItem from "@components/sidebar/items/ActiveTasksItem";
import ArchiveItem from "@components/sidebar/items/ArchiveItem";
import LeaveItem from "@components/sidebar/items/LeaveItem";
import SettingsItem from "@components/sidebar/items/SettingsItem";
import Sidebar from "@components/sidebar/Sidebar";
import { Column } from "@ismithi/ui-kit/components";
import Archive from "@pages/home/archive/Archive";
import HomePageLayout from "@pages/home/HomePage.layout";
import AddTaskListener from "@pages/home/listeners/AddTaskListener";
import TaskDoneListener from "@pages/home/listeners/TaskDoneListener";
import { EventDispatcherContext } from "@provider/EventDispatcherProvider";
import { TasksServiceContext } from "@provider/tasks/TasksServiceProvider";
import { observer, useLocalStore } from "mobx-react-lite";
import React, { useContext, useEffect } from "react";
import { Route } from "react-router-dom";

const defaultStore = {
  sideBarIsOpen: false,
  closeSidebar() {
    this.sideBarIsOpen = false;
  },
  openSidebar() {
    this.sideBarIsOpen = true;
  },
};

export const HomePageContext = React.createContext(defaultStore);

function HomePage() {
  const store = useLocalStore(() => defaultStore);

  const tasksService = useContext(TasksServiceContext);
  const eventDispatcher = useContext(EventDispatcherContext);

  useEffect(() => {
    eventDispatcher.addListener(new TaskDoneListener(tasksService));
    eventDispatcher.addListener(new AddTaskListener(tasksService));

    tasksService.getAll();

    return () => {
      eventDispatcher.removeAllListeners();
    };
  }, []);

  return (
    <HomePageContext.Provider value={store}>
      <Route exact path="/" component={HomePageLayout} />
      <Route exact path="/archive" component={Archive} />
      <Sidebar isOpen={store.sideBarIsOpen} onClose={store.closeSidebar}>
        <SettingsItem />
        <Route exact path="/" component={ArchiveItem} />
        <Route exact path="/archive" component={ActiveTasksItem} />
        <LeaveItem />
      </Sidebar>
    </HomePageContext.Provider>
  );
}

export default observer(HomePage);
