import React, { useEffect, useState } from "react";
import styles from "@pages/authentication/AuthenticationPage.styles.scss";
import cardStyles from "@styles/Card.styles.scss";
import animations from "@styles/Animations.scss";
import ICredentials from "@service/auth/ICredentials";
import { Input, Button } from "@ismithi/ui-kit/components";
import clsx from "clsx";
import Alert from "@components/alert/Alert";

interface IProps {
  loading?: boolean;
  onSubmit: (credentials: ICredentials) => void;
  error?: Error;
}

function AuthenticationPageLayout(props: IProps) {
  const [error, setError] = useState<Error>();
  const handleErrorClear = () => {
    setError(undefined);
  };

  useEffect(() => {
    setError(props.error);
  }, [props.error]);

  const handleFormSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    try {
      const formData: FormData = new FormData(e.target as HTMLFormElement);
      const credentials: ICredentials = {
        username: formData.get("username") as string,
        password: formData.get("password") as string,
      };

      props.onSubmit(credentials);
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <>
      <section className={styles.root}>
        <article className={styles.login_container}>
          <form className={styles.form_container} onSubmit={handleFormSubmit}>
            <div
              className={clsx(cardStyles.cardRoot, animations.fadeAndGrowIn)}
            >
              <div className={styles.title_container}>
                <h3>Authorize</h3>
              </div>

              <div className={styles.row}>
                <Input
                  label="Username"
                  name="username"
                  type="text"
                  disabled={props.loading}
                />
              </div>

              <div className={styles.row}>
                <Input
                  label="Password"
                  name="password"
                  type="password"
                  disabled={props.loading}
                />
              </div>

              <div className={styles.button_container}>
                <Button type="submit" disabled={props.loading}>
                  Log in
                </Button>
              </div>
            </div>
          </form>
        </article>
      </section>
      <Alert
        isOpen={typeof error !== "undefined"}
        onClose={handleErrorClear}
        title="Oops! Request failed"
        description={error?.message}
      />
    </>
  );
}

export default AuthenticationPageLayout;
