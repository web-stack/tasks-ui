import React, { useContext, useState } from "react";
import AuthenticationPageLayout from "./AuthenticationPage.layout";
import ICredentials from "@service/auth/ICredentials";
import { AuthService } from "@service/auth/AuthService";
import { AuthenticationContext } from "@provider/AuthenticationProvider";
import { RouterContext } from "@components/router/Router";

function AuthenticationPage() {
  const router = useContext(RouterContext);
  const authService: AuthService = useContext(AuthenticationContext);

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState();

  const handleSubmit = async (credentials: ICredentials) => {
    setLoading(true);
    let code;
    try {
      const result = await authService.authenticate(credentials);
      code = result.data.code;
    } catch (e) {
      setError(e);
    } finally {
      setLoading(false);
    }

    if (code === "ok") {
      router?.setRoute("/");
    }
  };

  return (
    <AuthenticationPageLayout
      onSubmit={handleSubmit}
      loading={loading}
      error={error}
    />
  );
}

export default AuthenticationPage;
