import { Task } from "@model/task/TaskModel";
import { action, computed, observable } from "mobx";

export interface ITasksStore {
  readonly archivedTasks: Task[];
  readonly tasks: Task[];

  setTasks(tasks: Task[]): void;
  setArchivedTasks(archivedTasks: Task[]): void;
}

export default class TasksStore {
  @observable
  private _tasks: Task[] = [];

  @observable
  private _archivedTasks: Task[] = [];

  @action("Add task")
  addTask = (task: Task) => {
    this._tasks.push(task);
  };

  @action("manually set tasks")
  setTasks(_tasks: Task[]) {
    this._tasks = _tasks;
  }

  @action
  setArchivedTasks(_tasks: Task[]) {
    this._archivedTasks = _tasks;
  }

  @computed
  get tasks() {
    return this._tasks;
  }

  @computed
  get archivedTasks() {
    return this._archivedTasks;
  }
}
