import prependZero from "@utils/prependZero";

/**
 * @param date
 *
 * @return string - date in string format dd.mm HH:ii
 */
export default function dateToRegularString(date: Date) {
  return !date
    ? ""
    : `${prependZero(date.getDate())}.${prependZero(
        date.getMonth()
      )} ${prependZero(date.getHours())}:${prependZero(date.getMinutes())}`;
}
