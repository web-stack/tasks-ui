import prependZero from "@utils/prependZero";

function dateToTextBasedFormat(date: Date) {
  const today = new Date();
  const words = [];

  if (
    today.getDate() === date.getDate() &&
    today.getMonth() === date.getMonth() &&
    today.getFullYear() === date.getFullYear()
  ) {
    words.push("today");
  } else if (
    today.getDate() === date.getDate() - 1 &&
    today.getMonth() === date.getMonth() &&
    today.getFullYear() === date.getFullYear()
  ) {
    words.push("tomorrow");
  } else {
    words.push(
      `${prependZero(date.getDate())}.${prependZero(date.getMonth() + 1)}`
    );
  }

  words.push(
    `at ${prependZero(date.getHours())}:${prependZero(date.getMinutes())}`
  );

  return words.join(" ");
}

export default dateToTextBasedFormat;
