import DispatchedEvent from "@utils/event/DispatchedEvent";

interface EventListener<T> {
  readonly name: string;
  onMessage(event: DispatchedEvent<T>): void;
}

export default EventListener;
