import DispatchedEvent from "@utils/event/DispatchedEvent";
import EventListener from "@utils/event/EventListener";

export interface IEventDispatcher {
  addListener<T>(listener: EventListener<T>): void;
  removeListener<T>(listener: EventListener<T>): void;
  removeAllListeners(): void;
  dispatch<T>(event: DispatchedEvent<T>): void;
}

class EventDispatcher implements IEventDispatcher {
  private listeners: EventListener<any>[] = [];

  dispatch<T>(event: DispatchedEvent<T>) {
    this.listeners.forEach((listener: EventListener<T>) => {
      if (event.name === listener.name) listener.onMessage(event);
    });
  }

  addListener<T>(listener: EventListener<T>): void {
    this.listeners.push(listener);
  }

  removeListener<T>(listener: EventListener<T>): void {
    const i = this.listeners.findIndex((l) => l === listener);
    this.listeners.splice(i, 1);
  }

  removeAllListeners() {
    this.listeners = [];
  }
}

export default EventDispatcher;
