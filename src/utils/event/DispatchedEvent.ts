interface DispatchedEvent<T> {
  name: string;
  payload: T;
}

export default DispatchedEvent;
