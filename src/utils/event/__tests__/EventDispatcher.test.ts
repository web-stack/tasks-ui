import DispatchedEvent from "@utils/event/DispatchedEvent";
import EventDispatcher from "@utils/event/EventDispatcher";
import EventListener from "@utils/event/EventListener";

describe("Event dispatcher test", function () {
  it("should add listeners and pass events", function () {
    const listener: EventListener<string> = {
      name: "update",
      onMessage<T>(event: DispatchedEvent<T>) {},
    };

    const method = spyOn(listener, "onMessage");

    const eventDispatcher = new EventDispatcher();
    eventDispatcher.addListener(listener);
    eventDispatcher.dispatch({
      name: "update",
      payload: "message",
    });

    expect(method).toBeCalled();
  });

  it("should invoke correct listener based on event name", function () {
    const listener: EventListener<string> = {
      name: "update",
      onMessage<T>(event: DispatchedEvent<T>) {},
    };
    const listener2: EventListener<string> = {
      name: "delete",
      onMessage<T>(event: DispatchedEvent<T>) {},
    };

    const method = spyOn(listener, "onMessage");
    const method2 = spyOn(listener2, "onMessage");

    const eventDispatcher = new EventDispatcher();
    eventDispatcher.addListener(listener);
    eventDispatcher.addListener(listener2);
    eventDispatcher.dispatch({
      name: "update",
      payload: "message",
    });

    expect(method).toBeCalled();
    expect(method2).not.toBeCalled();
  });
});
