export function getAccessToken() {
  return localStorage.getItem("access_token");
}

export function getDefaultHeaders() {
  return {
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
  };
}
