import React from "react";

type ReturnType = [boolean, (isOpen?: boolean) => void];

function useToggle(initialValue: boolean): ReturnType {
  const [state, setState] = React.useState<boolean>(initialValue);
  const toggle = (isOpen?: boolean) => {
    if (typeof isOpen === "undefined") {
      setState(!state);
      return;
    }

    setState(isOpen);
  };

  return [state, toggle];
}

export default useToggle;
