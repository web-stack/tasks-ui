import { AxiosResponse } from "axios";

export function throwErrorOnBadResponse(response: AxiosResponse) {
  if (response.status !== 200) {
    let error = new Error();
    error.name = "Wrong status";
    error.message = response.statusText || "unknown error";

    throw error;
  }

  if (!response.data.code) {
    let error = new Error();
    error.name = "Wrong code";
    error.message = "code is missing in response";

    throw error;
  }

  if (response.data.code !== "ok") {
    let error = new Error();
    error.name = "Code is not 'OK'";
    error.message =
      response.data.message || response.data.code || "unknown error";

    throw error;
  }

  return response;
}

export function extractData<T>(response: AxiosResponse<T>) {
  return response.data;
}
