function prependZero(num: number): string {
  if (num < 0) return num > -10 ? `-0${num}` : num + "";

  return num < 10 ? `0${num}` : num + "";
}

export default prependZero;
