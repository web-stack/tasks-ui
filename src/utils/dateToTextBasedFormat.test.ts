import dateToTextBasedFormat from "@utils/dateToTextBasedFormat";

describe("dateToTextBasedFormat test", function () {
  it("should return a date string with tomorrow", function () {
    const expireDate = new Date();
    expireDate.setDate(expireDate.getDate() + 1);
    expireDate.setHours(15);
    expireDate.setMinutes(30);

    const textDate = dateToTextBasedFormat(expireDate);

    expect(textDate).toContain("tomorrow");
    expect(textDate).toBe("tomorrow at 15:30");
  });

  it("should return a date string with today", function () {
    const expireDate = new Date();
    expireDate.setDate(expireDate.getDate());
    expireDate.setHours(15);
    expireDate.setMinutes(30);

    const textDate = dateToTextBasedFormat(expireDate);

    expect(textDate).toContain("today");
    expect(textDate).toBe("today at 15:30");
  });

  it("should return a date string as a regular string", function () {
    const expireDate = new Date();
    expireDate.setDate(3);
    expireDate.setMonth(2);
    expireDate.setHours(15);
    expireDate.setMinutes(30);

    const textDate = dateToTextBasedFormat(expireDate);

    expect(textDate).toBe("03.03 at 15:30");
  });

  it("should return date with correct date and month including timezone", function () {
    const expireDate = new Date("2020-06-03T14:38:27.136Z");

    const textDate = dateToTextBasedFormat(expireDate);

    expect(textDate).toBe("03.06 at 17:38");
  });
});
