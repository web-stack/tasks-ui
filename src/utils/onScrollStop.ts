const onScrollStop = (el: HTMLElement) =>
  function (callback: (e: Event) => void) {
    // Make sure a valid callback was provided
    if (!callback || typeof callback !== "function") return;

    // Setup scrolling variable
    let isScrolling: NodeJS.Timeout;

    // Listen for scroll events
    el.addEventListener(
      "scroll",
      function (e: Event) {
        // Clear our timeout throughout the scroll
        clearTimeout(isScrolling);

        // Set a timeout to run after scrolling ends
        isScrolling = setTimeout(function () {
          // Run the callback
          callback(e);
        }, 200);
      },
      false
    );
  };

export default onScrollStop;
