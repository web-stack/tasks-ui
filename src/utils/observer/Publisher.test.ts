import Observer from "@utils/observer/Observer";
import Publisher from "@utils/observer/Publisher";

describe("Publisher test", function () {
  it("should add subscriber as a raw function", function () {
    const logger = jest.fn();

    const publisher = new Publisher<string>();
    publisher.subscribe(logger);
    publisher.update("hello");

    expect(logger).toBeCalled();
  });

  it("should add subscriber as an object", function () {
    const logger: Observer<string> = {
      update(data: string) {
        console.log(data);
      },
    };

    const method = spyOn(logger, "update");

    const publisher = new Publisher<string>();
    publisher.subscribe(logger);
    publisher.update("hello");

    expect(method).toBeCalled();
  });

  it("should remove observer from pool", function () {
    function logger(data: string) {
      console.log(data);
    }

    const publisher = new Publisher<string>();
    publisher.subscribe(logger);

    expect(publisher.subscribers.length).toBe(1);

    publisher.unsubscribe(logger);

    expect(publisher.subscribers.length).toBe(0);
  });
});
