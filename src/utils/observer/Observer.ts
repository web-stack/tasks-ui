interface Observer<T> {
  update(data: T): void;
}

export type ObserverFunc<T> = (data: T) => void;

export default Observer;
