import Observer, { ObserverFunc } from "@utils/observer/Observer";

type ObserverType<T> = Observer<T> | ObserverFunc<T>;

class Publisher<T> {
  private observers: ObserverType<T>[] = [];

  public subscribe(observer: ObserverType<T>) {
    this.observers.push(observer);
  }

  public unsubscribe(observer: ObserverType<T>) {
    this.observers = this.observers.filter((o) => o !== observer);
  }

  public update(data: T) {
    this.observers.forEach((o) => {
      if (typeof o === "function") {
        o(data);
        return;
      }

      o.update(data);
    });
  }

  get subscribers(): ObserverType<T>[] {
    return [...this.observers];
  }
}

export default Publisher;
