import { Settings, settingsValidator } from "@model/settings/Settings";
import { action, observable, observe } from "mobx";
import { ValidationError } from "typed-contracts";

const defaultSettings: Settings = {
  tasksSortingOptions: {
    doneFirst: true,
    mostRecentFirst: true,
  },
};

export interface ISettingsService {
  readonly settings: Settings;
  loadSettings(): Promise<Settings>;
}

class SettingsService implements ISettingsService {
  @observable settings = defaultSettings;

  constructor() {
    this.settings = this.initializeSettings();
    observe(this.settings.tasksSortingOptions, (change) => {
      if (change.type === "update") {
        this.saveSettings();
      }
    });
  }

  private saveSettings() {
    const settingsJSON = JSON.stringify(this.settings);
    localStorage.setItem("settings", settingsJSON);
  }

  @action
  async loadSettings(): Promise<Settings> {
    const settingsJSON = localStorage.getItem("settings");

    let settings;
    if (!settingsJSON) {
      settings = this.initializeSettings();
    } else {
      try {
        settings = JSON.parse(settingsJSON);
      } catch (e) {
        return Promise.reject("Error while parsing settings");
      }
    }

    settings = settingsValidator(settings);
    if (settings instanceof ValidationError) {
      return Promise.reject("Failed to load settings");
    }

    this.settings = settings;
    console.log("SettingsService", "loaded settings");

    return Promise.resolve(this.settings);
  }

  private initializeSettings() {
    const defaultSettings: Settings = {
      tasksSortingOptions: {
        doneFirst: true,
        mostRecentFirst: true,
      },
    };

    localStorage.setItem("settings", JSON.stringify(defaultSettings));

    return defaultSettings;
  }
}

export default SettingsService;
