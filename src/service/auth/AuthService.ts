import ICredentials from "@service/auth/ICredentials";
import authenticationApi, {
  IAuthenticationApi,
} from "@service/auth/AuthServiceApi";
import { observable } from "mobx";

export class AuthService {
  @observable
  authenticated: boolean | null = null;
  private authenticationApi: IAuthenticationApi;

  constructor(authenticationApi: IAuthenticationApi) {
    this.authenticationApi = authenticationApi;
  }

  loadAccessToken() {
    return localStorage.getItem("access_token");
  }

  setAccessToken(accessToken: string) {
    localStorage.setItem("access_token", accessToken);
  }

  async authenticate(credentials: ICredentials) {
    const result = await this.authenticationApi.sendAuthentication(credentials);

    if (result.data.code !== "ok") {
      throw new Error(result.data.message);
    }

    this.setAccessToken(result.data.access_token);
    this.authenticated = true;

    return result;
  }

  async isAuthenticated() {
    const token = this.loadAccessToken();
    if (!token) {
      this.authenticated = false;
      return false;
    }

    const { data } = await this.authenticationApi.checkToken(token);

    this.authenticated = data.code === "ok";

    return this.authenticated;
  }

  logout() {
    localStorage.removeItem("access_token");
    this.authenticated = false;
  }
}

const authService = new AuthService(authenticationApi);

export default authService;
