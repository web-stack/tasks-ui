import { IAuthenticationApi } from "@service/auth/AuthServiceApi";
import ICredentials from "@service/auth/ICredentials";
import IAuthenticationResponse from "@service/auth/response/IAuthenticationResponse";
import { AxiosResponse } from "axios";
import ICheckTokenResponse from "@service/auth/response/ICheckTokenResponse";

export const authenticationApi: IAuthenticationApi = {
  checkToken(token: string): Promise<AxiosResponse<ICheckTokenResponse>> {
    return Promise.resolve({
      data: {
        code: "ok",
      },
      config: {},
      headers: {},
      request: {},
      status: 1,
      statusText: "ok",
    });
  },
  sendAuthentication(
    credentials: ICredentials
  ): Promise<AxiosResponse<IAuthenticationResponse>> {
    return Promise.resolve({
      data: {
        code: "ok",
        access_token: "some token",
      },
      config: {},
      headers: {},
      request: {},
      status: 1,
      statusText: "ok",
    });
  },
};
