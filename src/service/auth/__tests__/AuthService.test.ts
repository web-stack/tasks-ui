import { AuthService } from "@service/auth/AuthService";
import { authenticationApi } from "@service/auth/__tests__/StubAuthenticationApi";

describe("AuthService", function() {
  const authService = new AuthService(authenticationApi);

  it("should set and read access_token from localStorage", function() {
    authService.setAccessToken("some token");

    const token = authService.loadAccessToken();

    expect(token).toBe("some token");
  });

  it("should return success object if authentication passes", async function() {
    const result = await authService.authenticate({
      username: "smith",
      password: "12345678",
    });

    expect(result.data).toMatchObject({
      code: "ok",
      access_token: "some token",
    });
  });
});
