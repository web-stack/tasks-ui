export default interface IAuthenticationResponse {
  code: string;
  message?: string;
  access_token: string;
}
