export default interface ICheckTokenResponse {
  code: string;
}
