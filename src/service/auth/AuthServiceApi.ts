import ICredentials from "@service/auth/ICredentials";
import IAuthenticationResponse from "@service/auth/response/IAuthenticationResponse";
import ApiConfig from "@config/ApiConfig";
import axios, { AxiosResponse } from "axios";
import ICheckTokenResponse from "@service/auth/response/ICheckTokenResponse";

export interface IAuthenticationApi {
  sendAuthentication(
    credentials: ICredentials
  ): Promise<AxiosResponse<IAuthenticationResponse>>;

  checkToken(token: string): Promise<AxiosResponse<ICheckTokenResponse>>;
}

export class AuthServiceApi implements IAuthenticationApi {
  async sendAuthentication(credentials: ICredentials) {
    return axios.post(ApiConfig.auth.getToken, credentials);
  }

  async checkToken(token: string): Promise<AxiosResponse<ICheckTokenResponse>> {
    return axios.post(
      ApiConfig.auth.authorize,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
  }
}

const authenticationApi: IAuthenticationApi = new AuthServiceApi();

export default authenticationApi;
