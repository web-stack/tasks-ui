import { Task } from "@model/task/TaskModel";

export default interface IGetAllResponse {
  code: string;
  message?: string;
  payload: Task[];
}
