import { Task } from "@model/task/TaskModel";
import Validator from "@model/Validator";
import IGetAllResponse from "@service/tasks/response/IGetAllResponse";
import IGetArchivedResponse from "@service/tasks/response/IGetArchivedResponse";
import { ITasksServiceApi } from "@service/tasks/TasksServiceApi";
import { ValidationError } from "typed-contracts";

export interface ITasksServiceValidator extends ITasksServiceApi {}

class TasksServiceValidator implements ITasksServiceValidator {
  private validator: Validator<Task>;
  private tasksServiceApi: ITasksServiceApi;

  constructor(validator: Validator<Task>, tasksServiceApi: ITasksServiceApi) {
    this.validator = validator;
    this.tasksServiceApi = tasksServiceApi;
  }

  async add(task: Task): Promise<Task> {
    const validated = this.validator(task);
    if (validated instanceof ValidationError) {
      console.log(
        "TasksServiceValidator",
        "could not invoke tasksServiceApi.add method, reason: task is not valid"
      );
      console.log("TasksServiceValidator", validated);
      throw validated;
    }

    return await this.tasksServiceApi.add(task);
  }

  async getAll(): Promise<IGetAllResponse> {
    const result = await this.tasksServiceApi.getAll();

    result.payload = this.getValidTasks(result.payload);

    return result;
  }

  async getArchived(): Promise<IGetArchivedResponse> {
    const result = await this.tasksServiceApi.getArchived();

    result.payload = this.getValidTasks(result.payload);

    return result;
  }

  async markTaskAsDone(taskId: string): Promise<void> {
    return await this.tasksServiceApi.markTaskAsDone(taskId);
  }

  async archiveTask(taskId: string): Promise<void> {
    return await this.tasksServiceApi.archiveTask(taskId);
  }

  async deleteTask(taskId: string): Promise<void> {
    return await this.tasksServiceApi.deleteTask(taskId);
  }

  private getValidTasks(tasks: Task[]) {
    return tasks.filter((task) => {
      const validated = this.validator(task);
      if (validated instanceof ValidationError) {
        console.log("TasksServiceApi", "encountered invalid task model");
        console.log("TasksServiceApi", validated);
        return false;
      }

      return true;
    });
  }
}

export default TasksServiceValidator;
