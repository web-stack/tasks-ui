import { Task } from "@model/task/TaskModel";
import IGetAllResponse from "@service/tasks/response/IGetAllResponse";
import { throwErrorOnBadResponse } from "@utils/axios";
import { AxiosResponse } from "axios";
import { ITasksServiceApi } from "../TasksServiceApi";

const stubTasks: Task[] = [
  {
    _id: "1",
    done: false,
    expireDate: new Date().toISOString(),
    title: "Some title",
    description: "Description",
    archived: false,
  },
  {
    _id: "2",
    done: false,
    expireDate: new Date().toISOString(),
    title: "Some title",
    description: "Description",
    archived: false,
  },
  {
    _id: "3",
    done: false,
    expireDate: new Date().toISOString(),
    title: "Some title",
    description: "Description",
    archived: false,
  },
];

interface StubOptions extends Partial<IGetAllResponse> {}

export default class StubTasksServiceApi implements ITasksServiceApi {
  private readonly options: StubOptions;

  constructor(
    options: StubOptions = {
      code: "ok",
      payload: stubTasks,
      message: undefined,
    }
  ) {
    this.options = options;
  }

  async add(task: Task): Promise<Task> {
    return task;
  }

  async getAll(): Promise<IGetAllResponse> {
    const result = await Promise.resolve(this.options)
      .then((data) => {
        return {
          data,
          status: 200,
          statusText: "ok",
        } as AxiosResponse;
      })
      .then(throwErrorOnBadResponse);

    return result.data;
  }

  async getArchived(): Promise<IGetAllResponse> {
    const result = await Promise.resolve(this.options)
      .then((data) => {
        return {
          data: data.payload?.map((t) => ({ ...t, archived: true })),
          status: 200,
          statusText: "ok",
        } as AxiosResponse;
      })
      .then(throwErrorOnBadResponse);

    return result.data;
  }

  markTaskAsDone(taskId: string): Promise<void> {
    return Promise.resolve(undefined);
  }

  archiveTask(taskId: string): Promise<void> {
    return Promise.resolve(undefined);
  }

  deleteTask(taskId: string): Promise<void> {
    return Promise.resolve(undefined);
  }
}
