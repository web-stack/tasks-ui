import StubTasksServiceApi from "@service/tasks/__tests__/StubTasksServiceApi";
import TasksService from "@service/tasks/TasksService";
import TasksStore from "@store/tasks/TasksStore";

describe("TasksService test", function () {
  const tasksServiceApi = new StubTasksServiceApi();
  const tasksStore = new TasksStore();
  const tasksService = new TasksService(tasksServiceApi, tasksStore);

  it("Should load tasks", async () => {
    try {
      await tasksService.getAll();
    } catch (e) {
      console.log(e);
    }

    expect(tasksService.tasks).toBeTruthy();
    expect(tasksService.tasks.length).toBeGreaterThan(1);
  });
});

describe("TasksService stress test", function () {
  it("should catch error on bad response", async () => {
    const tasksServiceApi = new StubTasksServiceApi({ code: "error" });
    const tasksStore = new TasksStore();
    const tasksService = new TasksService(tasksServiceApi, tasksStore);

    let error;
    try {
      await tasksService.getAll();
    } catch (e) {
      error = e;
    }

    expect(error).toBeTruthy();
    expect(error.message).toBeTruthy();
  });
});
