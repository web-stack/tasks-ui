import { tasksValidator } from "@model/task/TaskModel";
import StubTasksServiceApi from "@service/tasks/__tests__/StubTasksServiceApi";
import TasksServiceValidator from "@service/tasks/TasksServiceValidator";

const invalidTasks: any[] = [
  {
    _id: "1",
    done: 0,
    expireDate: new Date(),
    title: "Some title",
    description: "Description",
  },
  {
    _id: "2",
    title: "Some title",
    description: "Description",
  },
  {
    done: false,
    expireDate: new Date(),
    title: "Some title",
    description: {},
  },
  {
    _id: "3",
    done: false,
    expireDate: new Date().toISOString(),
    title: "Some title",
    description: "Description",
    archived: false,
  },
];

describe("TasksServiceValidator", function () {
  const tasksServiceValidator = new TasksServiceValidator(
    tasksValidator,
    new StubTasksServiceApi({
      payload: invalidTasks,
      code: "ok",
      message: undefined,
    })
  );

  it("should remove invalid items from response object and return only valid ones", async function () {
    const tasks = await tasksServiceValidator.getAll();
    expect(tasks.payload.length).toBe(1);
  });
});
