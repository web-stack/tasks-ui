import ApiConfig from "@config/ApiConfig";
import { Task } from "@model/task/TaskModel";
import IGetAllResponse from "@service/tasks/response/IGetAllResponse";
import IGetArchivedResponse from "@service/tasks/response/IGetArchivedResponse";
import axios, { AxiosResponse } from "axios";
import { getDefaultHeaders } from "@utils/token";
import { throwErrorOnBadResponse } from "@utils/axios";

export interface ITasksServiceApi {
  getAll(): Promise<IGetAllResponse>;

  add(task: Task): Promise<Task>;

  markTaskAsDone(taskId: string): Promise<void>;

  archiveTask(taskId: string): Promise<void>;

  deleteTask(taskId: string): Promise<void>;

  getArchived(): Promise<IGetArchivedResponse>;
}

export default class TasksServiceApi implements ITasksServiceApi {
  /**
   * @throws {Error}
   */
  async getAll(): Promise<IGetAllResponse> {
    const response = (await axios
      .get(ApiConfig.server.getAll, getDefaultHeaders())
      .then(throwErrorOnBadResponse)) as AxiosResponse<IGetAllResponse>;

    return response.data;
  }

  async getArchived(): Promise<IGetArchivedResponse> {
    const response = (await axios
      .get(ApiConfig.server.getArchived, getDefaultHeaders())
      .then(throwErrorOnBadResponse)) as AxiosResponse<IGetArchivedResponse>;

    return response.data;
  }

  /**
   * @param task {Task}
   * @throws {Error}
   */
  async add(task: Task) {
    return await axios
      .put(ApiConfig.server.add, task, getDefaultHeaders())
      .then(throwErrorOnBadResponse)
      .then((result) => result.data.payload);
  }

  async markTaskAsDone(taskId: string): Promise<void> {
    await axios
      .post(ApiConfig.server.markAsDone, { taskId }, getDefaultHeaders())
      .then(throwErrorOnBadResponse);
  }

  async archiveTask(taskId: string): Promise<void> {
    await axios
      .post(ApiConfig.server.archive, { taskId }, getDefaultHeaders())
      .then(throwErrorOnBadResponse);
  }

  async deleteTask(taskId: string): Promise<void> {
    await axios
      .post(ApiConfig.server.deleteTask, { taskId }, getDefaultHeaders())
      .then(throwErrorOnBadResponse);
  }
}
