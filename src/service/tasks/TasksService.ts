import { Task } from "@model/task/TaskModel";
import { ITasksServiceApi } from "@service/tasks/TasksServiceApi";
import { ITasksStore } from "@store/tasks/TasksStore";

export interface ITasksService {
  readonly tasks: Task[];
  readonly archivedTasks: Task[];

  add(task: Task): Promise<void>;
  getAll(): Promise<Task[]>;
  getArchived(): Promise<Task[]>;
  markAsDone(taskId: string): Promise<void>;
  archive(taskId: string): Promise<void>;
  deleteTask(taskId: string): Promise<void>;
}

export default class TasksService implements ITasksService {
  private tasksStore: ITasksStore;
  private tasksServiceApi: ITasksServiceApi;

  constructor(tasksServiceApi: ITasksServiceApi, tasksStore: ITasksStore) {
    this.tasksStore = tasksStore;
    this.tasksServiceApi = tasksServiceApi;
  }

  async getAll(): Promise<Task[]> {
    const result = await this.tasksServiceApi.getAll();

    this.tasksStore.setTasks(result.payload);
    return this.tasksStore.tasks;
  }

  async getArchived(): Promise<Task[]> {
    const result = await this.tasksServiceApi.getArchived();

    this.tasksStore.setArchivedTasks(result.payload);
    return this.tasksStore.tasks;
  }

  async add(task: Task): Promise<void> {
    const newTask = await this.tasksServiceApi.add(task);

    this.tasksStore.tasks.push(newTask);
  }

  async markAsDone(taskId: string): Promise<void> {
    const task = this.tasksStore.tasks.find((t) => t._id === taskId);
    if (task) {
      task.done = true;
    }

    try {
      await this.tasksServiceApi.markTaskAsDone(taskId);
    } catch (e) {
      if (task) {
        task.done = false;
      }
      throw e;
    }
  }

  async archive(taskId: string) {
    const i = this.tasksStore.tasks.findIndex((t) => t._id === taskId);
    const task = this.tasksStore.tasks[i];

    this.tasksStore.tasks.splice(i, 1);

    try {
      await this.tasksServiceApi.archiveTask(taskId);
    } catch (e) {
      if (task) this.tasksStore.archivedTasks.push(task);

      throw e;
    }
  }

  async deleteTask(taskId: string) {
    const i = this.tasksStore.tasks.findIndex((t) => t._id === taskId);
    const task = this.tasksStore.tasks[i];

    this.tasksStore.tasks.splice(i, 1);

    try {
      await this.tasksServiceApi.deleteTask(taskId);
    } catch (e) {
      if (task) this.tasksStore.tasks.push(task);

      throw e;
    }
  }

  get tasks() {
    return this.tasksStore.tasks;
  }

  get archivedTasks() {
    return this.tasksStore.archivedTasks;
  }
}
