const oauthUrl = process.env.OAUTH_SERVER_URL || "";
const tasksServerUrl = process.env.TASKS_SERVER_URL || "";

const config = {
  auth: {
    getToken: `${oauthUrl}/oauth/token`,
    authorize: `${oauthUrl}/oauth/authorize`,
  },
  server: {
    getAll: `${tasksServerUrl}/tasks/all`,
    getArchived: `${tasksServerUrl}/tasks/getArchived`,
    add: `${tasksServerUrl}/tasks/add`,
    markAsDone: `${tasksServerUrl}/tasks/finish`,
    archive: `${tasksServerUrl}/tasks/archive`,
    deleteTask: `${tasksServerUrl}/tasks/delete`,
  },
};

export default config;
